#!/usr/bin/env sh

(
  find . -type f \( -name '*.bash' -o -name '*.sh' \)
  find . -type f -exec awk 'FNR == 1 && /^#!\s?\/(usr\/)?bin\/(env\s+)?(sh|bash)/ {print FILENAME}' {} \;
) | sort -u | (
  status=0
  while read -r file; do
    printf "Checking %s\n" "$file"
    errors=$(shellcheck --format=gcc "$file") || status=$?
    if [ -z "${errors}" ]; then
      printf "\e[32m%s\e[0m\n" "OK"
    else
      printf "\e[31m%s\e[0m\n" "$errors"
    fi
  done
  exit $status
)
